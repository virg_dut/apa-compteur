package fr.apa.compteur.utils;

/**
 * @author fabien
 */
public class ABR {

    private Mot word;

    private ABR gauche, droite;

    public ABR(Mot word) {
        this.word = word;
    }

    public void insert(Mot word) {
        ABR current = this, parent = this;

        while (current != null) {
            parent = current;
            if (current.getWord().getWord().charAt(0) <= word.getWord().charAt(0)) {
                current = current.getGauche();
            } else {
                current = current.getDroite();
            }
        }
        if (parent.getWord().getWord().charAt(0) <= word.getWord().charAt(0)) {
            parent.setGauche(new ABR(word));
        } else {
            parent.setDroite(new ABR(word));
        }
    }

    public Mot getWord() {
        return word;
    }

    public int getOccurence() {
        return this.word.getOccurrence();
    }

    public void addOccurence() {
        this.word.addOccurrence();
    }

    public ABR getGauche() {
        return gauche;
    }

    public ABR getDroite() {
        return droite;
    }

    public void setGauche(ABR gauche) {
        this.gauche = gauche;
    }

    public void setDroite(ABR droite) {
        this.droite = droite;
    }
    
}
