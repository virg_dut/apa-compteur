package fr.apa.compteur.utils;

public class Element {
	private String info;
	private Element suivant;
	public Element(String i, Element next) {
		info = i;
		suivant = next;
	}
	public Element(String i) {
		this(i,null);
	}
	public String getInfo() {
		return info;
	}
	public Element getSuivant() {
		return suivant;
	}
	public void setSuivant(Element e) {
		this.suivant=e;
	}
}

