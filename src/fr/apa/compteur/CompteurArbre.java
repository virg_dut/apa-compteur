package fr.apa.compteur;

import fr.apa.compteur.utils.ABR;
import fr.apa.compteur.utils.Mot;

import java.util.ArrayList;

/**
 * @author fabien
 */
public class CompteurArbre extends CompteurAbstract {

	public ABR arbre;

	public CompteurArbre(String file) {
		super(file);
	}

	/**
	 * it uses a stack
	 *
	 * @param word the word you want to increment the occurence
	 */
	@Override
	public void addOccurrence(String word) {
		ArrayList<ABR> stack = new ArrayList<>();
		ABR current;

		if (word.length() > 4) {
			if (arbre == null) {
				arbre = new ABR(new Mot(word));
				nbWords5++;
				nbWords++;
				return;
			} else getPath(word, stack, arbre);

			// while we have to look for something
			while (!stack.isEmpty()) {

				// get the first element of stack
				current = stack.remove(0);

				// if it is our word
				if (current.getWord().getWord().equals(word)) {
					current.addOccurence(); // increment it
					nbWords5++;
					nbWords++;
					return; // stop everything
				}

				getPath(word, stack, current);
			}

			// we didn't find it
			arbre.insert(new Mot(word));
			nbWords5++;
		}
		nbWords++;
	}

	private void getPath(String word, ArrayList<ABR> stack, ABR current) {
		if (current.getGauche() != null && current.getWord().getWord().charAt(0) <= word.charAt(0)) {
			stack.add(current.getGauche());
		} else if (current.getDroite() != null && current.getWord().getWord().charAt(0) > word.charAt(0)) {
			stack.add(current.getDroite());
		}
	}

	@Override
	public Mot[] getTop() {
		Mot[] mots = new Mot[10];

		ArrayList<ABR> stack = new ArrayList<>();

		boolean found;

		// index
		int i = 0;

		while (i < 10) {

			Mot max = null;
			ABR current = arbre;
			stack.add(current);

			while (!stack.isEmpty()) {
				// get the first element of stack
				current = stack.remove(0);

				if (max == null || current.getOccurence() > max.getOccurrence()) {
					found = false;
					for (Mot mot : mots) {
						if (mot != null && mot.getWord().equals(current.getWord().getWord())) {
							found = true;
							break;
						}
					}

					if (!found) {
						max = current.getWord();
					}
				}

				if (current.getGauche() != null)
					stack.add(current.getGauche());
				if (current.getDroite() != null)
					stack.add(current.getDroite());
			}
			mots[i++] = max;
		}
		return mots;
	}

}
